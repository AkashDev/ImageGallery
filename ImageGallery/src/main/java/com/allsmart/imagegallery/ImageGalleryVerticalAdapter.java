package com.allsmart.imagegallery;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

public class ImageGalleryVerticalAdapter extends RecyclerView.Adapter<ImageGalleryVerticalAdapter.ViewHolder> {

    Context context;

    public ImageGalleryVerticalAdapter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_gallery_vertical, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        context = parent.getContext();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (URLUtil.isValidUrl(GalleryManager.getImagePathList().get(position))) {
            Picasso.with(context).load(GalleryManager.getImagePathList().get(position)).into(holder.imageView);
        } else if (ImageFileFilter.accept(new File(GalleryManager.getImagePathList().get(position)))) {
            Bitmap bitmap = BitmapFactory.decodeFile(GalleryManager.getImagePathList().get(position));
            holder.imageView.setImageBitmap(bitmap);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ImageGallerySliderView.class);
                intent.putExtra("POS", position);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return GalleryManager.getImagePathList().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imgVerSingle);
        }
    }
}

