package com.allsmart.imagegallery;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

public class ImageGalleryVertical extends AppCompatActivity {

    public static ArrayList<String> imagePathList;
    RecyclerView verticalRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery_vertical);
        Log.i("Called", "ImageGalleryVertical");

//        populateImageList();

        verticalRecyclerView = findViewById(R.id.imageGalleryVertical);

        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(null, LinearLayoutManager.VERTICAL, false);
        verticalRecyclerView.setLayoutManager(layoutManager);
//        GalleryManager.setImagePathList(imagePathList);
        verticalRecyclerView.setAdapter(new ImageGalleryVerticalAdapter());
    }

    private void populateImageList() {
        imagePathList = new ArrayList<>();
        imagePathList.add("https://prodimage.images-bn.com/pimages/9780385541176_p0_v4_s192x300.jpg");
        imagePathList.add("https://prodimage.images-bn.com/pimages/9780385514231_p0_v5_s192x300.jpg");
        imagePathList.add("https://prodimage.images-bn.com/pimages/9780545790352_p0_v25_s192x300.jpg");
        imagePathList.add("https://prodimage.images-bn.com/pimages/9780062381736_p0_v2_s192x300.jpg");
        imagePathList.add("https://prodimage.images-bn.com/pimages/9781501139154_p0_v4_s118x184.jpg");

        File[] files = new File("storage/emulated/0/Image/").listFiles();
        if (files != null) {
            for (File f : files) {
                imagePathList.add(f.toString());
            }
        }
    }
}
