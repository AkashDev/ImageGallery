package com.allsmart.imagegallery;

import java.util.ArrayList;

public class GalleryManager {
    private static ArrayList<String> imagePathList;

    public GalleryManager() {

    }

    public static void setImagePathList(ArrayList<String> imagePathList) {
        GalleryManager.imagePathList = imagePathList;
    }

    public static ArrayList<String> getImagePathList() {
        return imagePathList;
    }
}
