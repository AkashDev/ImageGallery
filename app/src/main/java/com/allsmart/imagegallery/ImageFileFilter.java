package com.allsmart.imagegallery;

import java.io.File;

public class ImageFileFilter {
    static private final String[] acceptedFileExtensions = new String[]{"jpg", "png", "gif", "jpeg", ".bmp", ".webp"};

    /**
     * @param file File of which extension is to be checked
     * @return boolean true / false
     */
    public static boolean accept(File file) {
        for (String extension : acceptedFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension)) {
                return true;
            }
        }
        return false;
    }
}
