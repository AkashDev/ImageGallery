package com.allsmart.imagegallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class ImageGallerySliderViewAdapter extends PagerAdapter {

    private TouchImageView imageDisplay;

    public ImageGallerySliderViewAdapter() {
    }

    @Override
    public int getCount() {
        return GalleryManager.getImagePathList().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = layoutInflater.inflate(R.layout.image_gallery_slider, container, false);
        imageDisplay = viewLayout.findViewById(R.id.full_image);
        if (URLUtil.isValidUrl(GalleryManager.getImagePathList().get(position))) {
            Picasso.with(container.getContext())
                    .load(GalleryManager.getImagePathList().get(position))
                    .placeholder(R.drawable.loading)
                    .into(imageDisplay);
        } else if (ImageFileFilter.accept(new File(GalleryManager.getImagePathList().get(position)))) {
            Bitmap bitmap = BitmapFactory.decodeFile(GalleryManager.getImagePathList().get(position));
            imageDisplay.setImageBitmap(bitmap);
        }

        container.addView(viewLayout);
        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}