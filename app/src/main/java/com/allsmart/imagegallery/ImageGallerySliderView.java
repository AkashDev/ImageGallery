package com.allsmart.imagegallery;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class ImageGallerySliderView extends AppCompatActivity {
    public static int position;

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery_slider_view);

        viewPager = findViewById(R.id.view_pager);
        ArrayList<String> imagePathList = GalleryManager.getImagePathList();
        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            position = intent.getIntExtra("POS", 1);
        }

        GalleryManager.setImagePathList(imagePathList);
        ImageGallerySliderViewAdapter scrollViewAdapter = new ImageGallerySliderViewAdapter();
        viewPager.setAdapter(scrollViewAdapter);
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}